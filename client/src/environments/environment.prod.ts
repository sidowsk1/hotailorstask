const baseUrl = 'http://localhost:8080';
export const environment = {
  urls: {
    login: `${baseUrl}/login`,
    users: `${baseUrl}/users`
  },
  production: true
};
