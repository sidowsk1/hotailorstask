import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Credentials } from '../model/credentials.model';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private url = environment.urls;

  constructor(
    private http: HttpClient
  ) { }

  public login(credentials: Credentials): Observable<any> {
    const httpOptions: { withCredentials; } = {
      withCredentials: true
    };
    return this.http.post<Credentials>(`${this.url.login}`, credentials, httpOptions)
      .pipe(
        tap(() => sessionStorage.setItem('isLogged', 'true')));
  }

}
