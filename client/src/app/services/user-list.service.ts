import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class UserListService {

  private url = environment.urls;

  constructor(
    private http: HttpClient
  ) { }

  public getUserList(): Observable<any> {
    const httpOptions: { withCredentials; } = {
      withCredentials: true
    };
    return this.http.get<any>(`${this.url.users}`, httpOptions);
  }
}
