import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
  name: 'filter'
})
@Injectable()
export class FilterPipe implements PipeTransform {
  transform(users: any[], field: string, value: string): any[] {
    if (!users) {
      return [];
    }
    if (!field || !value) {
      return users;
    }

    return users.filter(user =>
      user[field].toLowerCase().includes(value.toLowerCase())
    );
  }
}