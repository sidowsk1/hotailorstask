import { Component, OnInit } from '@angular/core';
import { UserListService } from '../services/user-list.service';
import { AuthService } from '../services/auth.service';
import { User } from '../model/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  public userList: User[];
  public searchName: string;

  constructor(private service: UserListService) { }

  ngOnInit() {
    this.fetchUsers();
  }

  public fetchUsers() {
    this.service.getUserList().subscribe(response => {
      this.userList = response.data;
    });
  }

}
