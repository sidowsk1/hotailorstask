import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [

    { path: 'login', component: LoginComponent },
    { path: 'userList', component: UserListComponent, canActivate: [AuthGuard] },
    { path: '**', component: LoginComponent }

];

export const appRoutingModule = RouterModule.forRoot(routes);